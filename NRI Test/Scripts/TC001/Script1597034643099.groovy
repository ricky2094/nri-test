import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser(GlobalVariable.url)

WebUI.maximizeWindow()

WebUI.click(findTestObject('btnSkipSignin'))

WebUI.verifyTextPresent('Register', false)

WebUI.setText(findTestObject('txtFirstName'), 'ricky')

WebUI.setText(findTestObject('txtLastName'), 'julianto')

WebUI.setText(findTestObject('txtAddress'), 'jatijajar')

WebUI.setText(findTestObject('txtEmail'), 'rickyjn2094@gmail.com')

WebUI.setText(findTestObject('txtPhone'), '082156756542')

WebUI.click(findTestObject('RBMale'))

WebUI.check(findTestObject('checkbox2'))

WebUI.click(findTestObject('txtLanguage'))

WebUI.click(findTestObject('textIndonesian'))

WebUI.selectOptionByValue(findTestObject('selectSkill'), 'Engineering', false)

WebUI.selectOptionByValue(findTestObject('SelectCountries'), 'Indonesia', false)

WebUI.selectOptionByValue(findTestObject('selectYear'), '1994', false)

WebUI.selectOptionByValue(findTestObject('selectMonth'), 'July', false)

WebUI.selectOptionByValue(findTestObject('selectDay'), '20', false)

WebUI.setText(findTestObject('txtPassword'), '123qwe')

WebUI.setText(findTestObject('txtConfirmPassword'), '123qwe')

