<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>txtPhone</name>
   <tag></tag>
   <elementGuidId>56cd7d79-f0e2-4008-8e84-2400b1aeda72</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'form-control ng-pristine ng-untouched ng-invalid ng-invalid-required ng-valid-pattern']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control ng-pristine ng-untouched ng-invalid ng-invalid-required ng-valid-pattern</value>
   </webElementProperties>
</WebElementEntity>
