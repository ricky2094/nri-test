<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>txtAddress</name>
   <tag></tag>
   <elementGuidId>072ab4a3-4818-412a-a0f4-b7286c8c6891</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'form-control ng-pristine ng-valid ng-touched']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control ng-pristine ng-valid ng-touched</value>
   </webElementProperties>
</WebElementEntity>
