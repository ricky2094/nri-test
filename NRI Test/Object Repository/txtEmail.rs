<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>txtEmail</name>
   <tag></tag>
   <elementGuidId>f5bc6af4-8eab-46b5-aa43-66d94cfa252b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'form-control ng-pristine ng-untouched ng-valid-email ng-invalid ng-invalid-required']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control ng-pristine ng-untouched ng-valid-email ng-invalid ng-invalid-required</value>
   </webElementProperties>
</WebElementEntity>
