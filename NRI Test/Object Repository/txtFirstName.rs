<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>txtFirstName</name>
   <tag></tag>
   <elementGuidId>1080b4f2-c707-4539-8e26-e66c2090983d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'form-control ng-pristine ng-invalid ng-invalid-required ng-touched' and @ng-model = 'FirstName']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control ng-pristine ng-invalid ng-invalid-required ng-touched</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-model</name>
      <type>Main</type>
      <value>FirstName</value>
   </webElementProperties>
</WebElementEntity>
